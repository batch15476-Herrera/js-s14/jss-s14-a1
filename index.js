

let firstName = 'John';
console.log('First name: ${firstName}');
let lastName = 'Smith';
console.log('Last name: ${lastName}');
let age = 30;

let Hobbies = ['Biking', 'Mountain Climbing', 'Swimming'];

let address = {
	houseNumber: '32',
	street: 'Washington',
	city: 'Lincoln',
	state: 'Nebraska',
}

function printUserInfo(firstName, lastName, age, Hobbies, address){
	console.log(firstName)
	console.log(lastName)
	console.log(age)

	console.log('${firstName} ${lastName} is ${age} years of age.');

	console.log('This was printed inside of the function')

	console.log(Hobbies);

	console.log(address);
}

printUserInfo(firstName, lastName, age, Hobbies, address);

function returnFunction(){
	return true
}

let isMarried = returnFunction()
console.log('The value of isMarried is: ${isMarried}')